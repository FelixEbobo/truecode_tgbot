import asyncio
import logging
import sys
from telethon import TelegramClient, sync, events
from telethon.tl.types import Channel, User
from qrcode import QRCode
from truecode_tgbot.bot import main as start_bot
from truecode_tgbot.client_api import ClientAPI
from truecode_tgbot.settings import JsonSettings

def gen_qr(token:str):
    print(token)
    qr = QRCode()
    qr.clear()
    qr.add_data(token)
    qr.print_ascii()

async def main():
    settings = JsonSettings()
    api = ClientAPI(settings)
    await api.invite_to_channel(settings.get_setting('channel'), '+79834626819', 'client', '1')
    # qr_login = await client.qr_login()
    # gen_qr(qr_login.url)
    # await qr_login.wait()

    # dialogs = await client.get_dialogs()
    # for dialog in dialogs:
    #     if isinstance(dialog.entity, Channel):
    #         print(dialog)
    #         print(dialog.entity.id)
    #         print()
    #     if isinstance(dialog.entity, User):
    #         print(dialog)
    #         print(dialog.entity.id)
    #         print()


    # await client.send_message('felix_ebobo', 'Hello! Talking to you from Telethon')
    # await client.run_until_disconnected()

def setup_logging():
    logging.basicConfig(
        format="%(asctime)s [PID:%(process)d] %(levelname)s: %(message)s",
        level=logging.DEBUG,
        datefmt='%d-%b-%y %H:%M:%S')


if __name__ == '__main__':
    setup_logging()
    # asyncio.run(main())
    start_bot()
