
import asyncio
import datetime
from enum import Enum
import json
import logging
import os
from time import time
from typing import Dict
from truecode_tgbot.settings import JsonSettings
from truecode_tgbot.client_api import ClientAPI

class Syncer:
    class SyncStatus(Enum):
        FAILED = "failed"
        SUCCESS = "success"

    def __init__(self, client_api: ClientAPI, settings: JsonSettings) -> None:
        self.client = client_api
        self.sync_task: asyncio.Task = None
        self.wait_time = settings.get_setting('speed')
        self.dayly_limit = settings.get_setting('dayly_limit')
        self.channel_id = settings.get_setting("channel")["id"]
        with open('users.json', 'r', encoding='utf-8') as f:
            self.sync_data: Dict = json.load(f)

        if os.path.exists('metainfo.json'):
            with open('metainfo.json', 'r', encoding='utf-8') as f:
                self.metainfo = json.load(f)
        else:
            self.metainfo = {}

    def is_executing(self) -> bool:
        if self.sync_task:
            return not self.sync_task.done() and not self.sync_task.cancelled()
        return False

    def write_metainfo(self, phone: str, first_name: str, last_name: str, status: SyncStatus):
        self.metainfo[phone] = {
            "name": ' '.join((first_name, last_name)),
            "phone": phone,
            "status": status.value
        }

        with open('metainfo.json', 'w', encoding='utf-8') as f:
            json.dump(self.metainfo, f, ensure_ascii=False, indent=4)

    async def __syncer_task(self):
        self.start_time = time()
        await self.client.connect()
        count = 0
        element_number = len(self.sync_data.keys())
        logging.info(element_number)
        first_name = "client"
        last_name_num = count
        for phone, value in self.sync_data.items():
            logging.info(f"Adding user with phone number {phone}")
            if self.metainfo.get(phone) is not None:
                logging.debug("Phone is not None")
                element_number -= 1
                continue

            last_name = str(last_name_num)
            try:
                client_id = await self.client.add_user_to_contacts(phone, first_name, last_name)
                await self.client.invite_to_channel(self.channel_id, client_id)
                self.write_metainfo(client_id, phone, first_name, last_name, self.SyncStatus.SUCCESS)
            except Exception as e:
                logging.error(e)
                self.write_metainfo(0, phone, first_name, last_name, self.SyncStatus.FAILED)

            count += 1
            last_name_num += 1
            if count == self.dayly_limit:
                await self.client.disconnect()
                await self.__wait_til_next_day()
                element_number -= count
                count = 0
                await self.client.connect()
            elif count == element_number:
                logging.info("Finishing syncer task")
                break
            else:
                logging.info("Sleeping delay")
                await asyncio.sleep(self.wait_time * 60)
        logging.info("Finished syncer task")
        await self.client.disconnect()

    async def __wait_til_next_day(self):
        logging.info("Sleeping til the next day")
        now = time()
        next_day = (datetime.datetime.fromtimestamp(self.start_time) + datetime.timedelta(days=1)).timestamp()
        wait_time = next_day - now
        # await asyncio.sleep(wait_time)
        await asyncio.sleep(60)

    def start_syncer(self, callback):
        self.sync_task = asyncio.ensure_future(self.__syncer_task())
        self.sync_task.add_done_callback(callback)

    async def cancel(self):
        if self.sync_task and not self.sync_task.cancelled():
            await self.client.disconnect()
            self.sync_task.cancel()
        else:
            self.sync_task = None

