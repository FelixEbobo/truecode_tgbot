from enum import Enum

class Status(Enum):
    MENU = 0
    SETTINGS = 1
    SETTINGS_DAYLY_LIMIT = 2
    SETTINGS_SPEED = 3
    SETTINGS_AUTH = 4
    SETTINGS_CHANNEL = 5
    SYNC = 6

class User:

    def __init__(self, client_id: int):
        self.client_id = client_id
        self.status = Status.MENU

    def get_status(self) -> Status:
        return self.status

    def set_client_id(self, client_id: int):
        self.client_id = client_id

    def set_status_menu(self):
        self.status = Status.MENU

    def set_status_settings(self):
        self.status = Status.SETTINGS

    def set_status_settings_dayly_limit(self):
        self.status = Status.SETTINGS_DAYLY_LIMIT

    def set_status_settings_speed(self):
        self.status = Status.SETTINGS_SPEED

    def set_status_settings_auth(self):
        self.status = Status.SETTINGS_AUTH

    def set_status_settings_channel(self):
        self.status = Status.SETTINGS_CHANNEL

    def set_status_sync(self):
        self.status = Status.SYNC
