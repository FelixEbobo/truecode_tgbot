import json
import logging

class JsonSettings:
    """Class for settings manipulation"""

    def __init__(self):
        self.data = {}
        logging.debug("open setting.json file")
        with open('settings.json', 'r', encoding='utf-8') as f:
            logging.info("settings loaded")
            self.data = json.load(f)

    def get_setting(self, key: str, default = None) -> str | int | None:
        """read setting from settings.json under given key"""
        logging.debug(f"trying to get {key}")

        return self.data.get(key, default)

    def set_setting(self, key: str, value: str | int | None) -> None:
        """set value for setting under the given key from settings.json"""
        logging.debug(f"setting {key}: {value}")

        self.data[key] = value

        self.__save_settings_file()

    def __save_settings_file(self) -> None:
        logging.info("saving settings file")

        with open("settings.json", 'w', encoding="utf-8") as f:
            json.dump(self.data, f, indent=4, ensure_ascii=False)
