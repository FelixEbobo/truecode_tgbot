import logging
from typing import Dict
from telethon.tl.functions.channels import InviteToChannelRequest
from telethon.tl.functions.contacts import ImportContactsRequest, DeleteContactsRequest
from telethon.tl.types import InputPhoneContact, Channel, PeerUser
from telethon import TelegramClient

from qrcode import QRCode

from truecode_tgbot.settings import JsonSettings


class ClientAPI:
    def __init__(self, settings: JsonSettings) -> None:
        self.client: TelegramClient

        self.client = TelegramClient('first_session', settings.get_setting("app_id"), settings.get_setting("app_hash"), timeout=600)
        self.qr_login = None

    async def is_connected(self) -> bool:
        return await self.client.is_connected()

    async def connect(self) -> None:
        await self.client.connect()

    async def disconnect(self) -> None:
        await self.client.disconnect()

    def generate_qr_image(self):
        logging.info("Generating QR-Code")
        qr = QRCode()
        qr.add_data(self.qr_login.url)
        logging.debug(self.qr_login.url)
        qr.make()
        img = qr.make_image()
        logging.debug(img)
        with open("file.img", "wb") as f:
            img.save(f)
        logging.debug("saved")

    async def is_authorized(self) -> bool:
        return await self.client.is_user_authorized()

    async def authorize(self):
        logging.info("QR-Code logining")
        self.qr_login = await self.client.qr_login()
        logging.debug(self.qr_login)
        self.generate_qr_image()

    async def get_channel_by_name(self, channel_name: str) -> Dict[str, str | int] | None:
        logging.info(f"Trying to find channel with name {channel_name}")
        dialogs = await self.client.get_dialogs()
        for dialog in dialogs:
            logging.debug(dialog)
            if isinstance(dialog.entity, Channel):
                if dialog.entity.title == channel_name:
                    if dialog.entity.admin_rights.invite_users:
                        return {"name": dialog.entity.title, "id": dialog.entity.id}
        return None

    async def wait_authorize(self):
        await self.qr_login.wait(timeout=300)
    
    async def add_user_to_contacts(self, phone_number: str, first_name: str, last_name: str) -> int:
        logging.info("Adding user to contacts")
        response = await self.client(ImportContactsRequest([InputPhoneContact(client_id=0, phone=phone_number, first_name=first_name, last_name=last_name)]))
        logging.debug(response)
        return response.users[0].id

    async def invite_to_channel(self, channel_id: int, user_id: int):
        logging.info("Inviting user to channel")
        await self.client(InviteToChannelRequest(
            channel=channel_id,
            users=[user_id],
        ))

        await self.client(DeleteContactsRequest([user_id]))

    async def send_message(self, user_id: int, message: str):
        await self.client.send_message(PeerUser(user_id), message)
