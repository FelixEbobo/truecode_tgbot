from asyncio import exceptions
import asyncio
import logging
from telebot.types import Message
from telebot.async_telebot import AsyncTeleBot
from truecode_tgbot.syncer import Syncer

from truecode_tgbot.user import Status, User

from .settings import JsonSettings
from .keyboard import Keyboards

from truecode_tgbot.client_api import ClientAPI

def validate_dayly_limit(dayly_limit: int) -> bool:
    return dayly_limit > 0 and dayly_limit <= 200

def validate_speed(speed: int) -> bool:
    return speed > 0 and speed <= 20

def main():
    settings = JsonSettings()
    lang = settings.get_setting("lang")
    keyboardStorage = Keyboards(lang)
    client_api = ClientAPI(settings)
    syncer = Syncer(client_api, settings)

    messages = keyboardStorage.translation
    user = User(0)

    bot = AsyncTeleBot(settings.get_setting('bot_token'))

    @bot.message_handler(commands=["help", "start"])
    async def welcome_handler(message: Message):
        user.set_client_id(message.from_user.id)
        await bot.send_message(message.chat.id, text=messages['start_message'])
        await home_handler(message)

    @bot.message_handler(func=lambda msg: msg.text is not None and (msg.text == messages["inline_keyboard"]["home"] or msg.text == "/home"))
    async def home_handler(message: Message):
        await client_api.connect()
        settings.set_setting('auth', await client_api.is_authorized())
        await client_api.disconnect()

        user.set_status_menu()
        await bot.send_message(message.chat.id, text=messages['home'], reply_markup=keyboardStorage.basic_keyboard())

    async def current_settings(message: Message):
        authorized = settings.get_setting("auth", default=False)
        auth_message = "Авторизован" if authorized else "Не авторизован"
        await bot.send_message(message.chat.id,
            messages["current_settings"].format(
                settings.get_setting("dayly_limit"),
                settings.get_setting("speed"),
                auth_message,
                settings.get_setting("channel", {"name": "Нет канала"})["name"]))

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["keyboard"]["settings"])
    async def settings_handler(message: Message):
        user.set_status_settings()

        await current_settings(message)
        await bot.send_message(message.chat.id, messages['settings'], reply_markup=keyboardStorage.settings_keyboard())

    @bot.message_handler(func=lambda _: user.get_status() == Status.SETTINGS_DAYLY_LIMIT)
    async def process_dayly_limit(message):
        dayly_limit = message.text
        if not dayly_limit.isdigit():
            msg = await bot.reply_to(message, messages["error"]["dayly_limit"])
            return
        if not validate_dayly_limit(int(dayly_limit)):
            msg = await bot.reply_to(message, messages["error"]["dayly_limit"])
            return

        settings.set_setting("dayly_limit", int(dayly_limit))
        await settings_handler(message)

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["inline_keyboard"]["dayly_limit"])
    async def settings_dayly_limit_handler(message: Message):
        user.set_status_settings_dayly_limit()
        await bot.send_message(message.chat.id, messages["dayly_limit"])

    @bot.message_handler(func=lambda _: user.get_status() == Status.SETTINGS_SPEED)
    async def process_speed(message):
        speed = message.text
        if not speed.isdigit():
            await bot.reply_to(message, messages["error"]["speed"])
            return
        if not validate_speed(int(speed)):
            await bot.reply_to(message, messages["error"]["speed"])
            return

        settings.set_setting("speed", int(speed))
        await settings_handler(message)

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["inline_keyboard"]["speed"])
    async def settings_speed_handler(message: Message):
        user.set_status_settings_speed()
        await bot.send_message(message.chat.id, messages["speed"])

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["inline_keyboard"]["authorize_user"])
    async def settings_auth_handler(message: Message):
        user.set_status_settings_auth()
        await bot.send_message(message.chat.id, messages["auth"])
        await client_api.connect()
        await client_api.authorize()
        logging.debug("opening file.img")
        with open('file.img', 'rb') as f:
            logging.debug("Sending QR-Code")
            await bot.send_photo(message.chat.id, f)
        logging.debug("Waiting for authorization to complete")
        try:
            await client_api.wait_authorize()
        except exceptions.TimeoutError:
            await bot.send_message(message.chat.id, messages["auth_timeout"])
            await settings_handler(message)
            return

        settings.set_setting("auth", True)
        await bot.send_message(message.chat.id, messages["auth_success"])
        await client_api.disconnect()

        await settings_handler(message)

    @bot.message_handler(func=lambda _: user.get_status() == Status.SETTINGS_CHANNEL)
    async def process_channel(message):
        await client_api.connect()
        channel_name = message.text
        channel = await client_api.get_channel_by_name(channel_name)
        if channel:
            settings.set_setting("channel", channel)
            await settings_handler(message)
            return

        await bot.send_message(message.chat.id, messages["error"]["channel"])

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["inline_keyboard"]["channel"])
    async def settings_channel_handler(message: Message):
        user.set_status_settings_channel()
        await bot.send_message(message.chat.id, messages["channel"])

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["keyboard"]["start_sync"] and user.status == Status.MENU)
    async def start_sync_handler(message: Message):
        user.set_status_sync()
        await bot.send_message(message.chat.id, messages["start_sync"])
        syncer.start_syncer(lambda _: asyncio.ensure_future(bot.send_message(message.chat.id, messages["sync_is_done"])))
        await bot.send_message(message.chat.id, messages["start_sync_success"])

    @bot.message_handler(func=lambda msg: msg.text is not None and msg.text == messages["keyboard"]["stop_sync"])
    async def stop_sync_handler(message: Message):
        await bot.send_message(message.chat.id, messages["stop_sync"])
        if not syncer.is_executing():
            await bot.send_message(message.chat.id, messages["sync_is_not_running"])
        else:
            await syncer.cancel()
            await bot.send_message(message.chat.id, messages["stop_sync_success"])



    asyncio.get_event_loop().run_until_complete(bot.polling())