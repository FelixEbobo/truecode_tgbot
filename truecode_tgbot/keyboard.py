import json
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

class Keyboards:
    def __init__(self, lang: str = 'ru') -> None:
        self.translation = {}
        with open('messages.json', 'r', encoding='utf-8') as f:
            self.translation = json.load(f)[lang]
    
    def remove_keyboard(self):
        return ReplyKeyboardRemove()

    def basic_keyboard(self) -> ReplyKeyboardMarkup:
        keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)
        settings_btn = KeyboardButton(self.translation["keyboard"]["settings"])
        start_sync_btn = KeyboardButton(self.translation["keyboard"]["start_sync"])
        stop_sync_btn = KeyboardButton(self.translation["keyboard"]["stop_sync"])
        keyboard.add(settings_btn)
        keyboard.add(start_sync_btn)
        keyboard.add(stop_sync_btn)

        return keyboard

    def settings_keyboard(self) -> ReplyKeyboardMarkup:
        keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)
        keyboard_msg = self.translation["inline_keyboard"]
        dayly_limit_btn = KeyboardButton(keyboard_msg["dayly_limit"])
        speed_btn = KeyboardButton(keyboard_msg["speed"])
        channel_btn = KeyboardButton(keyboard_msg["authorize_user"])
        authorize_btn = KeyboardButton(keyboard_msg["channel"])
        home_btn = KeyboardButton(keyboard_msg["home"])
        keyboard.add(dayly_limit_btn)
        keyboard.add(speed_btn)
        keyboard.add(authorize_btn)
        keyboard.add(channel_btn)
        keyboard.add(home_btn)

        return keyboard
